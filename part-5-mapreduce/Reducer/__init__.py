import logging


def main(pair):
    word, counts = pair
    return (word, sum(counts))
