from collections import defaultdict


def main(pairs):
    d = defaultdict(list)
    for (word, count) in pairs:
        d[word].append(count)

    return [(k, v) for k, v in d.items()]
