def main(pair):
    line_no, text = pair
    return [(word, 1) for word in text.lower().split(" ")]
