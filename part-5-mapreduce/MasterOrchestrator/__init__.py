import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    # inputs = [
    #     (1, "the cat is"),
    #     (2, "the dog is"),
    #     (3, "my cat is")
    # ]
    inputs = yield context.call_activity("GetInputData", "dummy_input")

    map_tasks = [context.call_activity("Mapper", pair)
                 for pair in inputs]
    map_results = yield context.task_all(map_tasks)
    # Concatenate the (k,v) pairs produced by all mappers into one single list
    map_results = sum(map_results, [])

    sorted_res = yield context.call_activity("Shuffler", map_results)

    reduce_tasks = [context.call_activity("Reducer", pair)
                    for pair in sorted_res]
    reduce_results = yield context.task_all(reduce_tasks)
    
    return reduce_results

main = df.Orchestrator.create(orchestrator_function)