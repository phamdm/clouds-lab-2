import tempfile
from pathlib import Path

from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

AZURE_STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=storageaccmapreduce;AccountKey=l7k7AbzxxO18jZY31d0gCSO+GDqLlHwiXMzDBvDPS6gB7CAkA2yRZo+M6NRVOle9xidcwXtEg8XK+AStRAfDgA==;EndpointSuffix=core.windows.net"
CONTAINER_NAME = "input-text-container"

def main(dummyInput):
    try:
        blob_service_client = BlobServiceClient.from_connection_string(AZURE_STORAGE_CONNECTION_STRING)
        container_client = blob_service_client.get_container_client(container=CONTAINER_NAME) 

        data_dir = Path(tempfile.gettempdir()) / "data"
        data_dir.mkdir(exist_ok=True)

        # Download blobs
        blob_list = container_client.list_blobs()
        for blob in blob_list:
            with open(file=data_dir / blob.name, mode="wb") as f:
                f.write(container_client.download_blob(blob.name).readall())
        
        # Read each file and create a list of (linenum, text) tuples
        cnt = 0
        results = []
        for fname in data_dir.iterdir():
            with open(fname) as f:
                while True:
                    line = f.readline().strip()
                    if not line:
                        break
                    cnt += 1
                    results.append((cnt, line))
        return results
    except Exception as ex:
        print('Exception:')
        print(ex)
