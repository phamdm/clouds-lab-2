from locust import HttpUser, task


class QuickstartUser(HttpUser):
    # host = "http://localhost:5000"
    # host = "http://20.16.11.14"
    host = "https://webapp-integral.azurewebsites.net"
    # host = "https://function-integrate.azurewebsites.net/api/integrate"
    @task
    def load_test(self):
        self.client.get("?lower=0&upper=3.14157")
