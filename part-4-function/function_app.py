import azure.functions as func
from math import sin, pi


app = func.FunctionApp()

def get_param(req: func.HttpRequest, param_name, default_val=None):
    val = req.params.get(param_name)
    if not val:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            val = req_body.get(param_name)
    return val if val else default_val


@app.function_name(name="IntegrateFn")
@app.route(route="integrate", auth_level=func.AuthLevel.ANONYMOUS)
def integrate(req: func.HttpRequest) -> func.HttpResponse:
    lower = float(get_param(req, "lower", 0))
    upper = float(get_param(req, "upper", pi))
    if lower >= upper:
        return func.HttpResponse(f"lower={lower} must be smaller than upper={upper}",
                                 status_code=400)

    res_strs = [f"Integrating abs(sin(x)) from {lower} to {upper}:"]
    for n in [10, 100, 1000, 10000, 100000, 1000000]:
        dx = (upper - lower) / n
        res = 0
        for i in range(n):
            xi = dx * (i + 0.5)
            res += abs(sin(xi)) * dx
        res_strs.append(f"result={res}, n={n}")
    res_strs.append("")
    return func.HttpResponse("\n".join(res_strs), status_code=200)
