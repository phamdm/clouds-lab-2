from math import sin, pi

from flask import Flask, Response, request

app = Flask(__name__)

def abs_sin(x):
    return abs(sin(x))

def integrate(fn, lower, upper, n=10):
    dx = (upper - lower) / n
    res = 0

    for i in range(n):
        xi = dx * (i + 0.5)
        res += fn(xi) * dx
    return res

@app.route("/")
def main():
    lower = float(request.args.get("lower", 0))
    upper = float(request.args.get("upper", pi))
    if lower >= upper:
        return Response(f"lower={lower} must be smaller than upper={upper}", 400)

    res_strs = [f"Integrating abs(sin(x)) from {lower} to {upper}:"]
    for n in [10, 100, 1000, 10000, 100000, 1000000]:
        res = integrate(abs_sin,
                        lower=lower,
                        upper=upper,
                        n=n)
        res_strs.append(f"result={res}, n={n}")
    res_strs.append("")
    return "<br>".join(res_strs)
